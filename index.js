var through = require("through2");

module.exports = function(params)
{
    var setPipe = through.obj(
        function(file, encoding, callback)
        {
            // Set default variables.
            file.data.layout = file.data.page.layout || params.layout;
            file.data.engine = file.data.page.engine || params.engine;

            // gulp-layout requires layout to be a full path, not just a name.
            file.data.layout = `${params.prefix}${file.data.layout}${params.suffix}`;

            // Callback so we can continue.
            callback(null, file);
        });

    return setPipe;
}
